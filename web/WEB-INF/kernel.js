//объявление модуля 'app' - он содержит контроллеры, предоставляющие двустороннюю связь данных между html и js
var app = angular.module('app', []);

//в модуле app объявляется контроллер
app.controller('main', function ($scope) {
    /**
     * Объект области видимости для angular контроллера. Выполняет роль view-model
     */
    var sc = $scope;

    //массив, где мы храним перечень ID выбранных ответов.
    sc.selectedAnswers = [];

    //характеризует книгу, которую выбрал пользователь
    sc.selectedBook = {};

    //функция-сеттер выбора книги
    sc.selectBook = function(book) {
        sc.selectedBook = book;
    }

    //корневой вопрос со всеми подвопросами
    sc.root = {
        "id": 0,
        "desc": "С чего начнем?",
        "answers": {
            "Фантастика" : {
                "id" : 100,
                "name" : "Я, Робот",
                "author" : "Айзек Азимов",
                "answer" : "Фантастика",
                "answerPath" : [100]
            },
            "Фентези": {
                "id": 1,
                "desc": "Скучаете ли по Гарри Поттеру?",
                "answers": {
                    "Да": {
                        "id": 3,
                        "name": "Гарри Поттер и Философский Камень",
                        "author": "Дж.К. Роулинг",
                        "answer": "Да",
                        "answerPath": [1, 3]
                    },
                    "Нет": {
                        "id": 4,
                        "desc": "Вы неофит в фентези?",
                        "answers": {
                            "Да": {
                                "id": 5,
                                "name": "Властелин Колец",
                                "author": "Дж.Р.Р.Толкин",
                                "answer": "Да",
                                "answerPath": [1, 4, 5]
                            },
                            "Возможно": {
                                "id": 6,
                                "name": "Волшебник из Страны Оз",
                                "author": "Грегори Магвайр",
                                "answer": "Возможно",
                                "answerPath": [1, 4, 6]
                            },
                            "Нет": {
                                "id": 7,
                                "desc": "Любите легенды о короле Артуре?",
                                "answers": {
                                    "Да": {
                                        "id": 8,
                                        "desc": "И какой персонаж больше приглянулся?",
                                        "answers": {
                                            "Артур": {
                                                "id": 9,
                                                "name": "Король Былого и Грядущего",
                                                "author": "Г. Уайт",
                                                "answer": "Артур",
                                                "answerPath": [1, 4, 7, 8, 9]
                                            },
                                            "Мэрилин": {
                                                "id": 10,
                                                "name": "Хрустальный Грот",
                                                "author": "Мария Стюарт",
                                                "answer": "Мэрилин",
                                                "answerPath": [1, 4, 7, 8, 10]
                                            },
                                            "Феи Моргана": {
                                                "id": 11,
                                                "name": "Туманы Авалона",
                                                "author": "Марион Зиммер Бредли",
                                                "answer": "Феи Моргана",
                                                "answerPath": [1, 4, 7, 8, 11]
                                            }
                                        }
                                    },
                                    "Нет": {
                                        "id": 12,
                                        "desc": "Более современногго?",
                                        "answers": {
                                            "Да": {
                                                "id": 13,
                                                "desc": "Боги или люди под мостовыми?",
                                                "answers": {
                                                    "Современная Мифология": {
                                                        "id": 14,
                                                        "name": "Американские Боги",
                                                        "author": "Ник Гейман",
                                                        "answer": "Современная Мифология",
                                                        "answerPath": [1, 4, 7, 12, 13, 14]
                                                    },
                                                    "Мир под городом": {
                                                        "id": 15,
                                                        "name": "Задверье",
                                                        "author": "Ник Гейман",
                                                        "answer": "Мир под городом",
                                                        "answerPath": [1, 4, 7, 12, 13, 15]
                                                    }
                                                }
                                            },
                                            "Возможно": {
                                                "id": 16,
                                                "name": "Надвигается беда",
                                                "author": "Рэй Брэдбери",
                                                "answer": "Возможно",
                                                "answerPath": [1, 4, 7, 12, 16]
                                            },
                                            "Нет": {
                                                "id": 17,
                                                "desc": "Без ума от вестернов?",
                                                "answers": {
                                                    "Да": {
                                                        "id": 18,
                                                        "name": "Темная Башня",
                                                        "author": "Стивен Кинг",
                                                        "answer": "Да",
                                                        "answerPath": [1, 4, 7, 12, 17, 18]
                                                    },
                                                    "Нет": {
                                                        "id": 19,
                                                        "desc": "А к животным хорошо относитесь?",
                                                        "answers": {
                                                            "Да": {
                                                                "id": 20,
                                                                "desc": "И о каком питомце мечтаете?",
                                                                "answers": {
                                                                    "Единорог": {
                                                                        "id": 21,
                                                                        "name": "Последний Единорог",
                                                                        "author": "Питер Сойер Бигл",
                                                                        "answer": "Единорог",
                                                                        "answerPath": [1, 4, 7, 12, 17, 19, 20, 21]
                                                                    },
                                                                    "Кролик": {
                                                                        "id": 22,
                                                                        "name": "Обитатели Холмов",
                                                                        "author": "Ричард Адамс",
                                                                        "answer": "Кролик",
                                                                        "answerPath": [1, 4, 7, 12, 17, 19, 20, 22]
                                                                    },
                                                                    "Дракон": {
                                                                        "id": 23,
                                                                        "name": "Полет Дракона",
                                                                        "author": "Энн Маккефри",
                                                                        "answer": "Дракон",
                                                                        "answerPath": [1, 4, 7, 12, 17, 19, 20, 23]
                                                                    }
                                                                }
                                                            },
                                                            "Нет": {
                                                                "id": 24,
                                                                "desc": "Тогда может альтернативная история?",
                                                                "answers": {
                                                                    "Да": {
                                                                        "id": 25,
                                                                        "desc": "Романтика или сражающиеся волшебники?",
                                                                        "answers": {
                                                                            "Романтика": {
                                                                                "id": 26,
                                                                                "name": "Наследие",
                                                                                "author": "Жаклин Кэри",
                                                                                "answer": "Романтика",
                                                                                "answerPath": [1, 4, 7, 12, 17, 19, 24, 25, 26]
                                                                            },
                                                                            "Волшебники": {
                                                                                "id": 27,
                                                                                "name": "Джонатан Стрендж и мистер Нарелл",
                                                                                "author": "Сезан Кларк",
                                                                                "answer": "Волшебники",
                                                                                "answerPath": [1, 4, 7, 12, 17, 19, 24, 25, 27]
                                                                            }
                                                                        }
                                                                    },
                                                                    "Нет": {
                                                                        "id": 28,
                                                                        "desc": "Готовы взяться за серии книг?",
                                                                        "answers": {
                                                                            "Нет": {
                                                                                "id": 29,
                                                                                "desc": "С пиратами или без?",
                                                                                "answers": {
                                                                                    "К черту пиратов": {
                                                                                        "id": 30,
                                                                                        "name": "Звездная пыль",
                                                                                        "author": "Нил Гейнман",
                                                                                        "answer": "К черту пиратов",
                                                                                        "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 29, 30]
                                                                                    },
                                                                                    "Йо-хо-хо": {
                                                                                        "id": 31,
                                                                                        "name": "Принцесса-невеста",
                                                                                        "author": "Уилльям Голдман",
                                                                                        "answer": "Йо-хо-хо",
                                                                                        "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 29, 31]
                                                                                    }
                                                                                }
                                                                            },
                                                                            "Да": {
                                                                                "id": 32,
                                                                                "desc": "Серии завершены?",
                                                                                "answers": {
                                                                                    "Нет": {
                                                                                        "id": 33,
                                                                                        "desc": "Возвышенное или Низменное?",
                                                                                        "answers": {
                                                                                            "Низменное": {
                                                                                                "id": 34,
                                                                                                "name": "Песнь Льда и Огня",
                                                                                                "author": "Джордж Мартин",
                                                                                                "answer": "Низменное",
                                                                                                "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 32, 33, 34]
                                                                                            },
                                                                                            "Возвышенное": {
                                                                                                "id": 35,
                                                                                                "name": "Король Ветров",
                                                                                                "author": "Брендон Сандерсон",
                                                                                                "answer": "Возвышенное",
                                                                                                "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 32, 33, 35]
                                                                                            }
                                                                                        }
                                                                                    },
                                                                                    "Да": {
                                                                                        "id": 36,
                                                                                        "desc": "Фраза Sword and Sorcery вызывает ассоциации?",
                                                                                        "answers": {
                                                                                            "Нет": {
                                                                                                "id": 37,
                                                                                                "desc": "Старомодные трилогии?",
                                                                                                "answers": {
                                                                                                    "Да": {
                                                                                                        "id": 41,
                                                                                                        "name": "Ученик Чародея",
                                                                                                        "author": "Раймонд Фаст",
                                                                                                        "answer": "Да",
                                                                                                        "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 32, 36, 37, 41]
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //все книги
    sc.books = [{
        "id": 3,
        "name": "Гарри Поттер и Философский Камень",
        "author": "Дж.К. Роулинг",
        "answer": "Да",
        "answerPath": [1, 3],
        "$$hashKey": "object:10"
    }, {
        "id": 5,
        "name": "Властелин Колец",
        "author": "Дж.Р.Р.Толкин",
        "answer": "Да",
        "answerPath": [1, 4, 5],
        "$$hashKey": "object:16"
    }, {
        "id": 6,
        "name": "Волшебник из Страны Оз",
        "author": "Грегори Магвайр",
        "answer": "Возможно",
        "answerPath": [1, 4, 6],
        "$$hashKey": "object:20"
    }, {
        "id": 9,
        "name": "Король Былого и Грядущего",
        "author": "Г. Уайт",
        "answer": "Артур",
        "answerPath": [1, 4, 7, 8, 9],
        "$$hashKey": "object:28"
    }, {
        "id": 10,
        "name": "Хрустальный Грот",
        "author": "Мария Стюарт",
        "answer": "Мэрилин",
        "answerPath": [1, 4, 7, 8, 10],
        "$$hashKey": "object:32"
    }, {
        "id": 11,
        "name": "Туманы Авалона",
        "author": "Марион Зиммер Бредли",
        "answer": "Феи Моргана",
        "answerPath": [1, 4, 7, 8, 11],
        "$$hashKey": "object:36"
    }, {
        "id": 14,
        "name": "Американские Боги",
        "author": "Ник Гейман",
        "answer": "Современная Мифология",
        "answerPath": [1, 4, 7, 12, 13, 14],
        "$$hashKey": "object:44"
    }, {
        "id": 15,
        "name": "Задверье",
        "author": "Ник Гейман",
        "answer": "Мир под городом",
        "answerPath": [1, 4, 7, 12, 13, 15],
        "$$hashKey": "object:48"
    }, {
        "id": 16,
        "name": "Надвигается беда",
        "author": "Рэй Брэдбери",
        "answer": "Возможно",
        "answerPath": [1, 4, 7, 12, 16],
        "$$hashKey": "object:52"
    }, {
        "id": 18,
        "name": "Темная Башня",
        "author": "Стивен Кинг",
        "answer": "Да",
        "answerPath": [1, 4, 7, 12, 17, 18],
        "$$hashKey": "object:58"
    }, {
        "id": 21,
        "name": "Последний Единорог",
        "author": "Питер Сойер Бигл",
        "answer": "Единорог",
        "answerPath": [1, 4, 7, 12, 17, 19, 20, 21],
        "$$hashKey": "object:66"
    }, {
        "id": 22,
        "name": "Обитатели Холмов",
        "author": "Ричард Адамс",
        "answer": "Кролик",
        "answerPath": [1, 4, 7, 12, 17, 19, 20, 22],
        "$$hashKey": "object:70"
    }, {
        "id": 23,
        "name": "Полет Дракона",
        "author": "Энн Маккефри",
        "answer": "Дракон",
        "answerPath": [1, 4, 7, 12, 17, 19, 20, 23],
        "$$hashKey": "object:74"
    }, {
        "id": 26,
        "name": "Наследие",
        "author": "Жаклин Кэри",
        "answer": "Романтика",
        "answerPath": [1, 4, 7, 12, 17, 19, 24, 25, 26],
        "$$hashKey": "object:82"
    }, {
        "id": 27,
        "name": "Джонатан Стрендж и мистер Нарелл",
        "author": "Сезан Кларк",
        "answer": "Волшебники",
        "answerPath": [1, 4, 7, 12, 17, 19, 24, 25, 27],
        "$$hashKey": "object:86"
    }, {
        "id": 30,
        "name": "Звездная пыль",
        "author": "Нил Гейнман",
        "answer": "К черту пиратов",
        "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 29, 30],
        "$$hashKey": "object:94"
    }, {
        "id": 31,
        "name": "Принцесса-невеста",
        "author": "Уилльям Голдман",
        "answer": "Йо-хо-хо",
        "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 29, 31],
        "$$hashKey": "object:98"
    }, {
        "id": 34,
        "name": "Песнь Льда и Огня",
        "author": "Джордж Мартин",
        "answer": "Низменное",
        "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 32, 33, 34],
        "$$hashKey": "object:106"
    }, {
        "id": 35,
        "name": "Король Ветров",
        "author": "Брендон Сандерсон",
        "answer": "Возвышенное",
        "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 32, 33, 35],
        "$$hashKey": "object:110"
    }, {
        "id": 39,
        "name": "Рожденный туманом",
        "author": "Брендон Сандерсон",
        "answer": "О ворах",
        "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 32, 36, 37, 38, 39],
        "$$hashKey": "object:120"
    }, {
        "id": 40,
        "name": "Шанкара",
        "author": "Терри Брукс",
        "answer": "Про мангаффин",
        "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 32, 36, 37, 38, 40],
        "$$hashKey": "object:124"
    }, {
        "id": 41,
        "name": "Ученик Чародея",
        "author": "Раймонд Фаст",
        "answer": "Да",
        "answerPath": [1, 4, 7, 12, 17, 19, 24, 28, 32, 36, 37, 41],
        "$$hashKey": "object:128"
    }, {
        "id" : 100,
        "name" : "Я, Робот",
        "author" : "Айзек Азимов",
        "answer" : "Фантастика",
        "answerPath" : [100]
    }];

;

    //дерево подвопросов, начиная с текущего. При ответе на вопрос head смещается ниже. Сначала он равен корневому вопросу - корневому
    sc.head = sc.root;

    //функция выбора отвера
    sc.selectAnswer = function (answer) {
        //добавляем Id ответа в список выбранных ответов
        sc.selectedAnswers.push(answer.id);

        //сместим Head ниже на одну позицию
        sc.head = answer;
    };

    //сброс ответов
    sc.resetAnswers = function () {
        //вернем head обратно на корневой вопрос
        sc.head = sc.root;

        //почистим список выбранных ответов
        sc.selectedAnswers = [];
        sc.selectedBook = {};
    }

    //функция, возвращающая отфильтрованные книги
    sc.filteredBooks = function () {
        //результирующий массив
        var resultBooks = [];

        //перебираем все имеющиеся книги
        for (var i = 0; i < sc.books.length; i++) {
            //если какая-то книга не подходит, не добавляем ее в результат
            var contains = true;
            for (var j = 0; j < sc.selectedAnswers.length; j++) {
                if (sc.books[i].answerPath.indexOf(sc.selectedAnswers[j]) === -1) {
                    contains = false;
                }
            }
            if (contains) resultBooks.push(sc.books[i]);
        }
        if (resultBooks.indexOf(sc.selectedBook) === -1) {
            sc.selectedBook = {};
        };
        if (resultBooks.length === 1) {
            sc.selectedBook = resultBooks[0];
        }
        return resultBooks;
    }

    //вспомогательная функция. Возвращает перечень ID вопросов, на которые нужно ответить, чтобы получить в результате заданную книгу
    sc.extractAnswersPath = function (book) {
        //текущий путь
        var path = [];
        //верный путь
        var rightPath = [];
        var walkAnswers = function (answer) {
            path.push(answer.id);
            if (answer.id == book.id) {
                rightPath = path.slice();
                return;
            }
            for (ans in answer.answers) {
                //вызов рекурсивной функции
                walkAnswers(answer.answers[ans]);
            }
            path.pop();
        }

        for (ans in sc.root.answers) {
            walkAnswers(sc.root.answers[ans]);
        }
        book.answerPath = rightPath;
    };
});